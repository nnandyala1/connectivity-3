/*package com.consumer.controller;


import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.tranformxml.XSLTTranformation;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ XSLTTranformation.class, MessageRecieverApi.class, ActiveMQConnectionFactory.class, Queue.class, ConnectionFactory.class,
		Connection.class, Session.class, Destination.class, MessageConsumer.class, Message.class, TextMessage.class })
public class MessageRecieverApiTest {

	@Test
	public void testIfBlockMessageReceiver() {
		try {
			MessageRecieverApi mra = new MessageRecieverApi();
			ActiveMQConnectionFactory connectionFactory = PowerMockito.mock(ActiveMQConnectionFactory.class);
			Connection connection = PowerMockito.mock(Connection.class);
			Session session = PowerMockito.mock(Session.class);
			Queue queue = PowerMockito.mock(Queue.class);
			TextMessage textmessage = PowerMockito.mock(TextMessage.class);

			MessageConsumer consumer = createMock(MessageConsumer.class);
			PowerMockito.whenNew(ActiveMQConnectionFactory.class)
					.withArguments("admin", "admin", "tcp://devopsapp.southcentralus.cloudapp.azure.com:61616")
					.thenReturn(connectionFactory);
			PowerMockito.when(connectionFactory.createConnection()).thenReturn(connection);
			PowerMockito.when(connection.createSession(Mockito.any(Boolean.class), Mockito.any(Integer.class)))
					.thenReturn(session);
			PowerMockito.when(session.createQueue(Mockito.anyString())).thenReturn(queue);
			PowerMockito.when(session.createConsumer(queue)).thenReturn(consumer);
			PowerMockito.mockStatic(XSLTTranformation.class);
			PowerMockito.doNothing().when(XSLTTranformation.class,"trasformXML", "dummy message");

			expect(consumer.receive()).andReturn(textmessage);
			expect(consumer.receive()).andReturn(null);
			replay(consumer);
			Whitebox.setInternalState(MessageRecieverApi.class, "consumer", consumer);

			mra.connectionFactory();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/

	/*@Test
	public void testElseBlockMessageReceiver() {
		try {
			ActiveMQConnectionFactory connectionFactory = PowerMockito.mock(ActiveMQConnectionFactory.class);
			Connection connection = PowerMockito.mock(Connection.class);
			Session session = PowerMockito.mock(Session.class);
			Queue queue = PowerMockito.mock(Queue.class);
			//TextMessage textmessage = PowerMockito.mock(TextMessage.class);
			Message message = PowerMockito.mock(Message.class);

			MessageConsumer consumer = createMock(MessageConsumer.class);
			PowerMockito.whenNew(ActiveMQConnectionFactory.class)
					.withArguments("admin", "admin", "tcp://devopsapp.southcentralus.cloudapp.azure.com:61616")
					.thenReturn(connectionFactory);
			PowerMockito.when(connectionFactory.createConnection()).thenReturn(connection);
			PowerMockito.when(connection.createSession(Mockito.any(Boolean.class), Mockito.any(Integer.class)))
					.thenReturn(session);
			PowerMockito.when(session.createQueue(Mockito.anyString())).thenReturn(queue);
			PowerMockito.when(session.createConsumer(queue)).thenReturn(consumer);

			expect(consumer.receive()).andReturn(message);
			expect(consumer.receive()).andReturn(null);
			replay(consumer);
			Whitebox.setInternalState(MessageRecieverApi.class, "consumer", consumer);

			MessageRecieverApi.messageReceiver();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}*/
